/**
 * University of San Diego
 * COMP 285
 * Spring 2015
 * Instructor: Gautam Wilkins
 *
 * This class implements Huffman codes for files containing ASCII characters. It can read and compress a file and also
 * decode a compressed file and covert it back to its original ASCII characters.
 */

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {

        // Read characters from input file
        ArrayList<Character> characterArrayList = HuffmanCode.charactersFromFile("aaa.txt");
        String fileContents = "";
        for (Character c : characterArrayList) {
            fileContents = fileContents.concat(c.toString());
        }

        // You'll need to implement this method
        HashMap<Character, Integer> freq = HuffmanCode.frequenciesFromArray(characterArrayList);
        // You'll need to implement this method
        HuffmanTree fullTree = HuffmanCode.generateCodingTree(freq);

        // Generate encodings for the characters
        HashMap<Character, ArrayList<Boolean>> encoding = fullTree.getEncodings();
        String encodedString = HuffmanCode.encodeCharacters(characterArrayList, encoding);

        // Convert binary strings to byte arrays
        byte[] byteArray = BinaryHelper.byteArrayFromBitString(encodedString);
        byte[] huffmanTreeEncoding = HuffmanTree.huffmanTreeToByteArray(fullTree);

        // Write byte arrays to file
        BinaryHelper.writeBytesToFile("test2-htree.txt", huffmanTreeEncoding);
        BinaryHelper.writeBytesToFile("test2-encoded.txt", byteArray);

        // Decode strings from file and check that it matches the original encoded string
        byte[] testByteArray = BinaryHelper.readBytesFromFile("test2-encoded.txt");
        String encodedStringFromFile = BinaryHelper.bitStringFromByteArray(testByteArray);

        byte[] huffmanTreeEncoded = BinaryHelper.readBytesFromFile("test2-htree.txt");
        HuffmanTree treeFromFile = HuffmanTree.huffmanTreeFromByteArray(huffmanTreeEncoded);

        // You'll need to implement this method
        String original = HuffmanTree.decodeBinaryString(encodedStringFromFile, treeFromFile);

        // This should print 0 once your implementation is working
        System.out.println(original.compareTo(fileContents));





    }
}
